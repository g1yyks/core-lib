package ru.grendiser;

import org.junit.jupiter.api.BeforeAll;
import org.slf4j.MDC;

import java.util.UUID;

public class TestCoreLib {

    @BeforeAll
    public static void setUpLogData() {
        MDC.put("TRN_ID", UUID.randomUUID().toString());
        MDC.put("USER", "junit-test");
        MDC.put("SYSTEM", "CORE-LIB");
    }
}
