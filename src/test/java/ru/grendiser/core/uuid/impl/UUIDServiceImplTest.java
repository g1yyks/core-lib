package ru.grendiser.core.uuid.impl;

import org.junit.jupiter.api.Test;
import ru.grendiser.TestCoreLib;
import ru.grendiser.core.uuid.UUIDService;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class UUIDServiceImplTest extends TestCoreLib {

    UUIDService uuidService = new UUIDServiceImpl();

    @Test
    void generateUUID() {
        //Given
        UUID uuid = UUID.randomUUID();
        //When
        UUID generated = uuidService.generateUUID(uuid.toString());
        //Then
        assertAll(
                () -> assertNotNull(generated),
                () -> assertEquals(uuid, generated)
        );
    }

    @Test
    void whenStringIsNull_generateRandomUUID() {
        //Given
        //When
        UUID generated = uuidService.generateUUID(null);
        //Then
        assertNotNull(generated);
    }

    @Test
    void whenStringIsEmpty_generateRandomUUID() {
        //Given
        //When
        UUID generated = uuidService.generateUUID("");
        //Then
        assertNotNull(generated);
    }

    @Test
    void whenStringIsNotCorrectUUID_generateUUIDFromString() {
        //Given
        String isNotUUID = "is not uuid";
        //When
        UUID generated = uuidService.generateUUID(isNotUUID);
        //Then
        assertNotNull(generated);
    }
}