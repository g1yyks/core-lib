package ru.grendiser.core.filter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.domain.FilterType;
import ru.grendiser.core.filter.domain.factory.AuthorFactory;
import ru.grendiser.core.filter.domain.factory.BookFactory;
import ru.grendiser.core.filter.domain.factory.FilterFactory;
import ru.grendiser.core.filter.domain.factory.FilterRequestFactory;
import ru.grendiser.core.filter.domain.factory.VersionFactory;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@EnableAutoConfiguration
@SpringBootTest(classes = SpringStarter.class)
@ContextConfiguration
class ContainsFilterIT {

    private static final String STEPHEN_KING = "Stephen Edwin King";
    private static final String DESCRIPTION = "description of book";
    private static final String TITLE = "title of book";
    private static final String AUTHOR_NAME_FIELD = "name";
    private static final String AUTHOR_MIDDLE_NAME = "Edwin";
    private static final String UNNAMED_AUTHOR = "test author";
    private static final String VERSION_OF_BOOK_1 = "version of book 1";
    private static final String VERSION_OF_BOOK_2 = "version of book 1";

    @Autowired
    private AuthorRepository authorRepository;

    @Transactional
    @Test
    void loadAuthorsWithBookFilteredByContainsFilterByAuthorName() {
        //Given
        List<Author> authors = List.of(
                getAuthorWithBooks(VERSION_OF_BOOK_1, UNNAMED_AUTHOR)
                , getAuthorWithBooks(VERSION_OF_BOOK_2, STEPHEN_KING));
        authorRepository.saveAll(authors);

        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                        .withField(AUTHOR_NAME_FIELD)
                        .withType(FilterType.CONTAINS)
                        .withValue(AUTHOR_MIDDLE_NAME)
                        .build()
                ).build();

        //When
        List<Author> foundDocument = authorRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(1, foundDocument.size())

        );
    }

    private Author getAuthorWithBooks(String versionOfBook, String authorName) {
        return new AuthorFactory()
                .withName(authorName)
                .withBooks(
                        List.of(new BookFactory()
                                .withDate(LocalDate.now())
                                .withDescription(DESCRIPTION)
                                .withName(TITLE)
                                .withVersions(
                                        List.of(new VersionFactory()
                                                .withNumber(versionOfBook)
                                                .withPublishDate(LocalDate.now())
                                                .build())
                                )
                                .build())
                )
                .build();
    }
}
