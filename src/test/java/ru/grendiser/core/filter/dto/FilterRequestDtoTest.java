package ru.grendiser.core.filter.dto;

import org.junit.jupiter.api.Test;
import ru.grendiser.core.json.JsonConverter;
import ru.grendiser.core.json.impl.JsonConverterImpl;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FilterRequestDtoTest {
    private static final String FILTER_JSON = "/json/filters/filter.json";
    private final JsonConverter converter = new JsonConverterImpl();

    @Test
    void whenJsonWithFilterRequest_converterToPojo_thenOK() throws IOException {
        //Given
        String filterRequestJson = new String(this.getClass().getResourceAsStream(FILTER_JSON).readAllBytes());
        //When
        FilterRequestDto filterReq = converter.serialization(filterRequestJson, FilterRequestDto.class);
        //Then
        assertAll(
                () -> assertNotNull(filterReq),
                () -> assertEquals(10, filterReq.getPage().getSize()),
                () -> assertEquals(5, filterReq.getPage().getNumber()),
                () -> assertEquals(2, filterReq.getFilters().size()),
                () -> assertAll(
                        () -> assertEquals("document.attachments.status", filterReq.getFilters().get(0).getField()),
                        () -> assertEquals("value", filterReq.getFilters().get(0).getValue()),
                        () -> assertEquals(FilterTypeDto.EQ, filterReq.getFilters().get(0).getType()),
                        () -> assertEquals(2, filterReq.getFilters().get(0).getValues().size())
                )
        );
    }

}