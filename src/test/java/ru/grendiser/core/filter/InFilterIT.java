package ru.grendiser.core.filter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.domain.FilterType;
import ru.grendiser.core.filter.domain.factory.AuthorFactory;
import ru.grendiser.core.filter.domain.factory.BookFactory;
import ru.grendiser.core.filter.domain.factory.FilterFactory;
import ru.grendiser.core.filter.domain.factory.FilterRequestFactory;
import ru.grendiser.core.filter.domain.factory.VersionFactory;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@EnableAutoConfiguration
@SpringBootTest(classes = SpringStarter.class)
@ContextConfiguration
class InFilterIT {

    private static final String STEPHEN_KING = "Stephen Edwin King";
    private static final String DESCRIPTION = "description of book";
    private static final String TITLE = "title of book";
    private static final String VERSION_BOOK_FIELD = "books.versions.number";
    private static final String UNNAMED_AUTHOR = "test author";
    private static final String VERSION_OF_BOOK_1 = "version of book 1";
    private static final String VERSION_OF_BOOK_2 = "version of book 2";

    @Autowired
    private AuthorRepository authorRepository;

    @Transactional
    @Test
    void loadAuthorsWithBookFilteredByBookDate_whenDateGrater() {
        //Given
        LocalDate firstBookDate = LocalDate.now().minusMonths(1);
        LocalDate secondBookDate = LocalDate.now().minusMonths(2);
        List<Author> authors = List.of(
                getAuthorWithBooks(VERSION_OF_BOOK_1, UNNAMED_AUTHOR, firstBookDate)
                , getAuthorWithBooks(VERSION_OF_BOOK_2, STEPHEN_KING, secondBookDate));
        authorRepository.saveAll(authors);

        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                        .withField(VERSION_BOOK_FIELD)
                        .withType(FilterType.IN)
                        .withValues(VERSION_OF_BOOK_1, VERSION_OF_BOOK_2)
                        .build()
                ).build();

        //When
        List<Author> foundDocument = authorRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(2, foundDocument.size())

        );
    }

    private Author getAuthorWithBooks(String versionOfBook, String authorName, LocalDate bookDate) {
        return new AuthorFactory()
                .withName(authorName)
                .withBooks(
                        List.of(new BookFactory()
                                .withDate(bookDate)
                                .withDescription(DESCRIPTION)
                                .withName(TITLE)
                                .withVersions(
                                        List.of(new VersionFactory()
                                                .withNumber(versionOfBook)
                                                .withPublishDate(LocalDate.now())
                                                .build())
                                )
                                .build())
                )
                .build();
    }
}
