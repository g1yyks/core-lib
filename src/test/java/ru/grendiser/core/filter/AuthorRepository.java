package ru.grendiser.core.filter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.Document;

@Repository
public interface AuthorRepository  extends JpaRepository<Author, Long>, JpaSpecificationExecutor<Author> {
}
