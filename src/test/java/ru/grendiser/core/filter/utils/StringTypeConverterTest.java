package ru.grendiser.core.filter.utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.grendiser.core.filter.utils.StringTypeConverter.getValues;


class StringTypeConverterTest {

    @Test
    void whenStringIsNumberAndFieldIsNotString_returnLong() {
        //Given
        String number = "100024";
        Long expected = 100024L;
        //When
        List<Long> value = getValues(Long.class, List.of(number));
        //Then
        assertEquals(expected, value.get(0));
    }

    @Test
    void whenStringIsNumberAndFieldIsString_returnString() {
        //Given
        String number = "100024";
        //When
        List<String> value = getValues(String.class, List.of(number));
        //Then
        assertEquals(number, value.get(0));
    }

    @Test
    void whenStringIsDate_returnLocalDate() {
        //Given
        String date = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        //When
        List<LocalDate> convertedDate = getValues(LocalDate.class, List.of(date));
        //Then
        assertEquals(LocalDate.now(), convertedDate.get(0));
    }

    @Test
    void givenStringAsDateTime_whenConvert_returnLocalDateTime() {
        //Given
        LocalDateTime dateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        String date = dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        //When
        List<LocalDateTime> convertedDateTime = getValues(LocalDateTime.class, List.of(date));
        //Then
        assertEquals(dateTime, convertedDateTime.get(0));
    }

    @Test
    void givenStringAsBoolean_whenConvert_returnBoolean() {
        //Given
        String bool = Boolean.toString(true);
        //When
        List<Boolean> convertedBool = getValues(Boolean.class, List.of(bool));
        //Then
        assertEquals(Boolean.valueOf(bool), convertedBool.get(0));
    }

    @Test
    void givenStringAsString_whenConvert_returnStringInLowerCase() {
        //Given
        String value = "Any Value 001 JS";
        //When
        List<String> converted = getValues(String.class, List.of(value));
        //Then
        assertEquals(value, converted.get(0));
    }

    @Test
    void givenStringAsDateTimeAndBool_whenConvert_returnStrings() {
        //Given
        LocalDateTime dateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        String date = dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        //When
        List<String> convertedDateTime = getValues(LocalDateTime.class, List.of(date, Boolean.TRUE.toString()));
        //Then
        assertEquals(date, convertedDateTime.get(0));
        assertEquals(Boolean.TRUE.toString(), convertedDateTime.get(1));
    }

    @Test
    void givenStringAsBooleanAndInt_whenConvert_returnBStrings() {
        //Given
        String bool = Boolean.toString(true);
        int countOfAnything = 10;
        //When
        List<String> convertedBool = getValues(Boolean.class, List.of(bool, Integer.toString(countOfAnything)));
        //Then
        assertEquals(bool, convertedBool.get(0));
        assertEquals(Integer.toString(countOfAnything), convertedBool.get(1));
    }

    @Test
    void whenStringIsDateAndBoolean_returnStrings() {
        //Given
        String date = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        //When
        List<String> convertedDate = getValues(LocalDate.class, List.of(date, Boolean.TRUE.toString()));
        //Then
        assertEquals(date, convertedDate.get(0));
        assertEquals(Boolean.TRUE.toString(), convertedDate.get(1));
    }

    @Test
    void whenStringIsNumberAndBoolean_returnStrings() {
        //Given
        String number = "100024";
        //When
        List<String> value = getValues(Long.class, List.of(number, Boolean.TRUE.toString()));
        //Then
        assertEquals(number, value.get(0));
        assertEquals(Boolean.TRUE.toString(), value.get(1));
    }
}