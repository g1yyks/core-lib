package ru.grendiser.core.filter.mapper;

import org.junit.jupiter.api.Test;
import ru.grendiser.core.filter.domain.Filter;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.dto.FilterDto;
import ru.grendiser.core.filter.dto.FilterRequestDto;
import ru.grendiser.core.filter.dto.FilterTypeDto;
import ru.grendiser.core.filter.dto.PageDto;
import ru.grendiser.core.filter.dto.SortDto;
import ru.grendiser.core.filter.dto.SortTypeDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FilterRequestMappingTest {

    @Test
    void filterRequestMapping() {
        //Given
        FilterRequestDto requestDto = FilterRequestDto.builder()
                .filters(List.of(
                        FilterDto.builder()
                                .type(FilterTypeDto.EQ).value("test").build()))
                .page(PageDto.builder()
                        .number(10)
                        .size(100)
                        .sort(
                                SortDto.builder()
                                        .field("sort_field")
                                        .type(SortTypeDto.DESC)
                                        .build()
                        )
                        .build())
                .build();
        //When
        FilterRequest request = FilterRequestMapping.MAPPER.dtoToDomain(requestDto);
        //Then
        assertAll(
                () -> assertNotNull(request)
        );
    }
}