package ru.grendiser.core.filter.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import ru.grendiser.core.filter.domain.Filter;
import ru.grendiser.core.filter.dto.FilterDto;
import ru.grendiser.core.filter.dto.FilterTypeDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FilterMapperTest {

    private static final String FIELD = "field";
    private static final String VALUE_1 = "value1";
    private static final String VALUE_2 = "value2";

    @ParameterizedTest
    @EnumSource(FilterTypeDto.class)
    void mappingDtoToDomainWithOneValue(FilterTypeDto filterTypeDto) {
        //Given
        FilterDto filterDto = FilterDto.builder()
                .field(FIELD)
                .type(filterTypeDto)
                .value(VALUE_1)
                .build();
        //When
        Filter filter = FilterMapper.MAPPER.dtoToDomain(filterDto);
        //Then
        assertAll(
                () -> assertEquals(filterDto.getField(), filter.getField()),
                () -> assertEquals(filterDto.getType().name(), filter.getType().name()),
                () -> assertEquals(filterDto.getValue(), filter.getValue())
        );
    }

    @ParameterizedTest
    @EnumSource(FilterTypeDto.class)
    void mappingDtoToDomainWithTwoValue(FilterTypeDto filterTypeDto) {
        //Given
        FilterDto filterDto = FilterDto.builder()
                .field(FIELD)
                .type(filterTypeDto)
                .values(List.of(VALUE_1, VALUE_2))
                .build();
        //When
        Filter filter = FilterMapper.MAPPER.dtoToDomain(filterDto);
        //Then
        assertAll(
                () -> assertEquals(filterDto.getField(), filter.getField()),
                () -> assertEquals(filterDto.getType().name(), filter.getType().name()),
                () -> assertEquals(2, filter.getValues().size())
        );
    }

}