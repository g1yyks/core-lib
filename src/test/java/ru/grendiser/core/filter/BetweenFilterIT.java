package ru.grendiser.core.filter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.domain.FilterType;
import ru.grendiser.core.filter.domain.factory.AuthorFactory;
import ru.grendiser.core.filter.domain.factory.BookFactory;
import ru.grendiser.core.filter.domain.factory.FilterFactory;
import ru.grendiser.core.filter.domain.factory.FilterRequestFactory;
import ru.grendiser.core.filter.domain.factory.VersionFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional
@EnableAutoConfiguration
@SpringBootTest(classes = SpringStarter.class)
@ContextConfiguration
class BetweenFilterIT {

    private static final String STEPHEN_KING = "Stephen Edwin King";
    private static final String DESCRIPTION = "description of book";
    private static final String TITLE = "title of book";
    private static final String BOOK_DATE_FIELD = "books.date";
    private static final String UNNAMED_AUTHOR = "test author";
    private static final String VERSION_OF_BOOK_1 = "version of book 1";
    private static final String VERSION_OF_BOOK_2 = "version of book 1";

    @Autowired
    private AuthorRepository authorRepository;

    @Transactional
    @Test
    void loadAuthorsWithBookFilteredByBookDate_BetweenNowAndDateOfPast() {
        //Given
        LocalDate firstBookDate = LocalDate.now().minusMonths(1);
        LocalDate secondBookDate = LocalDate.now().minusMonths(2);
        LocalDate thirdBookDate = LocalDate.now().minusMonths(3);
        List<Author> authors = List.of(
                getAuthorWithBooks(VERSION_OF_BOOK_1, UNNAMED_AUTHOR, firstBookDate)
                , getAuthorWithBooks(VERSION_OF_BOOK_2, STEPHEN_KING, secondBookDate)
                , getAuthorWithBooks(VERSION_OF_BOOK_1, STEPHEN_KING, thirdBookDate));
        authorRepository.saveAll(authors);

        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                        .withField(BOOK_DATE_FIELD)
                        .withType(FilterType.BETWEEN)
                        .withValues(LocalDate.now().toString(), LocalDate.now().minusMonths(4).toString())
                        .build()
                ).build();
        //When
        List<Author> foundDocument = authorRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(3, foundDocument.size())

        );
    }

    @Transactional
    @Test
    void loadAuthorsWithBookFilteredByBookDate_throwExceptionIfSendOnlyOneElement() {
        //Given
        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                        .withField(BOOK_DATE_FIELD)
                        .withType(FilterType.BETWEEN)
                        .withValues(LocalDate.now().toString())
                        .build()
                ).build();
        //When //Then
        assertThrows(NoSuchElementException.class, () -> authorRepository.findAll(filterRequest));
    }

    @Test
    void loadAuthorsWithBookFilteredByBookDate_throwExceptionIfNotSendElements() {
        //Given
        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                        .withField(BOOK_DATE_FIELD)
                        .withType(FilterType.BETWEEN)
                        .withValues()
                        .build()
                ).build();
        //When //Then
        assertThrows(NoSuchElementException.class, () -> authorRepository.findAll(filterRequest));
    }

    private Author getAuthorWithBooks(String versionOfBook, String authorName, LocalDate bookDate) {
        return new AuthorFactory()
                .withName(authorName)
                .withBooks(
                        List.of(new BookFactory()
                                .withDate(bookDate)
                                .withDescription(DESCRIPTION)
                                .withName(TITLE)
                                .withVersions(
                                        List.of(new VersionFactory()
                                                .withNumber(versionOfBook)
                                                .withPublishDate(LocalDate.now())
                                                .build())
                                )
                                .build())
                )
                .build();
    }
}
