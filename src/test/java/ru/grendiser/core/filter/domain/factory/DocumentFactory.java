package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.Document;
import ru.grendiser.core.filter.domain.User;

import java.util.List;

public class DocumentFactory {

    private Document document = new Document();

    public DocumentFactory withNumber(Long number) {
        document.setNumber(number);
        return this;
    }

    public DocumentFactory withName(String name) {
        document.setName(name);
        return this;
    }

    public DocumentFactory withUsers(List<User> users) {
        document.setUsers(users);
        return this;
    }

    public Document build() {
        return document;
    }
}
