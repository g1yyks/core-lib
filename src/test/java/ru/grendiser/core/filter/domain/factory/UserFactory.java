package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.User;

public class UserFactory {

    private User user = new User();

    public UserFactory withName(String name) {
        user.setFio(name);
        return this;
    }

    public User build() {
        return user;
    }
}


