package ru.grendiser.core.filter.domain.factory;

import org.springframework.data.domain.PageRequest;
import ru.grendiser.core.filter.domain.BaseDomain;
import ru.grendiser.core.filter.domain.Filter;
import ru.grendiser.core.filter.domain.FilterRequest;

import java.util.List;

public class FilterRequestFactory<T extends BaseDomain> {

    FilterRequest<T> request = new FilterRequest<T>();

    /**
     * private List<Filter<T>> filters = new ArrayList<>();
     *     private PageRequest page;
     */

    public FilterRequestFactory<T> withFilter(Filter<T> filter) {
        request.setFilters(List.of(filter));
        return this;
    }

    public FilterRequestFactory<T> withFilters(List<Filter<T>> filters) {
        request.setFilters(filters);
        return this;
    }

    public FilterRequestFactory<T> withPage(PageRequest page) {
        request.setPage(page);
        return this;
    }

    public FilterRequest<T> build() {
        return request;
    }

}
