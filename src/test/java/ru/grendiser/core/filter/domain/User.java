package ru.grendiser.core.filter.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter @Setter
@Entity
public class User extends BaseDomain {
    private String fio;
}
