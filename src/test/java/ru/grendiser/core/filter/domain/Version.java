package ru.grendiser.core.filter.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.time.LocalDate;

@Getter @Setter
@Entity
public class Version extends BaseDomain{
    private LocalDate publishDate;
    private String number;
}
