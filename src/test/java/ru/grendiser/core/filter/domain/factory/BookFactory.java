package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.Book;
import ru.grendiser.core.filter.domain.Version;

import java.time.LocalDate;
import java.util.List;

public class BookFactory {

    private Book book = new Book();
    /*
    book.setDate(LocalDate.now());
        book.setDescription("asdfasdfasd");
        book.setName("titile");
        book.setVersions(List.of(documentVersion));
     */

    public BookFactory withDate(LocalDate date) {
        book.setDate(date);
        return this;
    }

    public BookFactory withDescription(String description) {
        book.setDescription(description);
        return this;
    }

    public BookFactory withName(String title) {
        book.setName(title);
        return this;
    }

    public BookFactory withVersions(List<Version> versions) {
        book.setVersions(versions);
        return this;
    }

    public Book build() {
        return book;
    }
}
