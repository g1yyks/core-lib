package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.Version;

import java.time.LocalDate;

public class VersionFactory {

    private Version versionOfBook = new Version();

    /*
    documentVersion.setNumber(number);
        documentVersion.setPublishDate(LocalDate.now());
     */

    public VersionFactory withNumber(String numberVersion) {
        versionOfBook.setNumber(numberVersion);
        return this;
    }

    public VersionFactory withPublishDate(LocalDate publishDate) {
        versionOfBook.setPublishDate(publishDate);
        return this;
    }

    public Version build() {
        return versionOfBook;
    }
}
