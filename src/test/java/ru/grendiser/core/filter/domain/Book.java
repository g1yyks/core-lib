package ru.grendiser.core.filter.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Getter @Setter
@Entity
public class Book extends BaseDomain{
    private String name;
    private String description;
    private LocalDate date;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Version> versions;
}
