package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.BaseDomain;
import ru.grendiser.core.filter.domain.Filter;
import ru.grendiser.core.filter.domain.FilterType;

import java.util.List;

public class FilterFactory<T extends BaseDomain> {

    private Filter<T> filter = new Filter<T>();

    public FilterFactory<T> withField(String fieldPath) {
        filter.setField(fieldPath);
        return this;
    }

    public FilterFactory<T> withType(FilterType filterType) {
        filter.setType(filterType);
        return this;
    }

    public FilterFactory<T> withType(String filterTypeName) {
        filter.setType(FilterType.valueOf(filterTypeName));
        return this;
    }

    public FilterFactory<T> withValue(String value) {
        filter.setValue(value);
        return this;
    }

    public FilterFactory<T> withValues(String...values) {
        filter.setValues(List.of(values));
        return this;
    }

    public Filter<T> build() {
        return filter;
    }
}
