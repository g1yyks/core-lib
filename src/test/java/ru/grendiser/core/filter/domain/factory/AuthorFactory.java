package ru.grendiser.core.filter.domain.factory;

import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.Book;

import java.util.List;

public class AuthorFactory {

    private Author author = new Author();

    public AuthorFactory withBooks(List<Book> books) {
        author.setBooks(books);
        return this;
    }

    public AuthorFactory withName(String name) {
        author.setName(name);
        return this;
    }

    public Author build() {
        return author;
    }

}
