package ru.grendiser.core.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.grendiser.core.filter.domain.Author;
import ru.grendiser.core.filter.domain.Document;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.domain.FilterType;
import ru.grendiser.core.filter.domain.User;
import ru.grendiser.core.filter.domain.factory.AuthorFactory;
import ru.grendiser.core.filter.domain.factory.BookFactory;
import ru.grendiser.core.filter.domain.factory.DocumentFactory;
import ru.grendiser.core.filter.domain.factory.FilterFactory;
import ru.grendiser.core.filter.domain.factory.FilterRequestFactory;
import ru.grendiser.core.filter.domain.factory.UserFactory;
import ru.grendiser.core.filter.domain.factory.VersionFactory;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@EnableAutoConfiguration
@SpringBootTest(classes = SpringStarter.class)
@ContextConfiguration
class EqFilterIT {

    private static final String STEPHEN_KING = "Stephen Edwin King";
    private static final String DESCRIPTION = "description of book";
    private static final String TITLE = "title of book";
    private static final String USER_NAME = "user name";
    private static final String DOCUMENT_NAME = "document name";
    private static final String DOCUMENT_NAME_FIELD = "name";
    private static final String USER_FIO_FIELD = "users.fio";
    private static final String BOOKS_VERSIONS_NUMBER_FIELD = "books.versions.number";
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private AuthorRepository authorRepository;

    @Test
    void loadDataFromDBByFilter() {
        //Given
        createDocumentForUser();
        FilterRequest<Document> filterRequest = new FilterRequestFactory<Document>()
                .withFilter(new FilterFactory<Document>()
                        .withField(DOCUMENT_NAME_FIELD)
                        .withType(FilterType.EQ)
                        .withValue(DOCUMENT_NAME)
                        .build()
                ).build();
        //When
        List<Document> foundDocument = documentRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(1, foundDocument.size()),
                () -> assertEquals(filterRequest.getFilters().get(0).getValue(), foundDocument.get(0).getName())
        );
    }


    @Test
    void loadDataFromDBByUserNameFilter() {
        //Given
        createDocumentForUser();
        FilterRequest<Document> filterRequest = new FilterRequestFactory<Document>()
                .withFilter(new FilterFactory<Document>()
                        .withField(USER_FIO_FIELD)
                        .withType(FilterType.EQ)
                        .withValue(USER_NAME)
                        .build()
                ).build();
        //When
        List<Document> foundDocument = documentRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(1, foundDocument.size()),
                () -> assertEquals(filterRequest.getFilters().get(0).getValue(), foundDocument.get(0).getUsers().get(0).getFio())
        );
    }

    @Transactional
    @Test
    void loadAuthorsWithBookFilteredByVersion() {
        //Given
        String firstVersionOfBook = "first";
        String secondVersionOfBook = "second";
        Author author = getAuthorWithBooks(firstVersionOfBook);
        Author author2 = getAuthorWithBooks(secondVersionOfBook);
        authorRepository.saveAll(List.of(author, author2));

        FilterRequest<Author> filterRequest = new FilterRequestFactory<Author>()
                .withFilter(new FilterFactory<Author>()
                    .withField(BOOKS_VERSIONS_NUMBER_FIELD)
                    .withType(FilterType.EQ)
                    .withValue(firstVersionOfBook)
                    .build()
                ).build();

        //When
        List<Author> foundDocument = authorRepository.findAll(filterRequest);
        //Then
        assertAll(
                () -> assertEquals(1, foundDocument.size())

        );
    }

    private Author getAuthorWithBooks(String versionOfBook) {
        return new AuthorFactory()
                .withName(STEPHEN_KING)
                .withBooks(
                        List.of(new BookFactory()
                                    .withDate(LocalDate.now())
                                    .withDescription(DESCRIPTION)
                                    .withName(TITLE)
                                    .withVersions(
                                            List.of(new VersionFactory()
                                                    .withNumber(versionOfBook)
                                                    .withPublishDate(LocalDate.now())
                                                    .build())
                                            )
                                    .build())
                )
                .build();
    }

    private void createDocumentForUser() {
        User user = new UserFactory().withName(USER_NAME).build();
        documentRepository.save(
                new DocumentFactory()
                    .withName(DOCUMENT_NAME)
                    .withNumber(1L)
                    .withUsers(List.of(user)).build());
    }
}
