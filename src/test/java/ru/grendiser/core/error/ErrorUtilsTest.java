package ru.grendiser.core.error;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;
import ru.grendiser.TestCoreLib;
import ru.grendiser.core.error.dto.ErrorDto;
import ru.grendiser.core.error.impl.ErrorUtilsImpl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static ru.grendiser.core.error.impl.ErrorUtilsImpl.GENERAL_ERROR_MESSAGE;

class ErrorUtilsTest extends TestCoreLib {

    private static final String ERROR_BUNDLE = "utils.error";
    private static final String ERROR_ID = "error.id";
    private static final String WARNING_ID = "warning.id";
    private static final String USER_ID = "user-id";
    private static final String USER_ACTION = "user-action";

    private final ErrorUtils errorUtils = new ErrorUtilsImpl();

    @BeforeEach
    public void setUp() {
        Locale.setDefault(new Locale("ru", "RU"));
    }

    @Test
    void createError() {
        //Given
        //When
        ErrorDto error = errorUtils.createError(ERROR_ID);
        //Then
        assertError(null, error, ERROR_ID, ErrorDto.Severity.ERROR);
    }

    @Test
    void createErrorWithDefaultErrorMessage() {
        //Given
        String errorIdWithoutMessage = "error.id.without.message";
        //When
        ErrorDto error = errorUtils.createError(errorIdWithoutMessage);
        //Then
        assertAll(
                () -> assertNotNull(error),
                () -> assertEquals(errorIdWithoutMessage, error.getId()),
                () -> assertEquals(ErrorDto.Severity.ERROR, error.getSeverity()),
                () -> assertEquals(GENERAL_ERROR_MESSAGE, error.getMessage())
        );
    }

    @Test
    void createErrorWithIncorrectLocale() {
        //Given
        Locale.setDefault(new Locale("en", "US"));
        //When
        ErrorDto error = errorUtils.createError(ERROR_ID);
        //Then
        assertError(null, error, ERROR_ID, ErrorDto.Severity.ERROR);
    }

    @Test
    void createErrorWithAdditionalParameters() {
        //Given
        Map<String, String> additionalParams = getAdditionalProperties();
        //When
        ErrorDto error = errorUtils.createError(ERROR_ID, additionalParams);
        //Then
        assertError(additionalParams, error, ERROR_ID, ErrorDto.Severity.ERROR);
    }

    @Test
    void createWarning() {
        //Given
        //When
        ErrorDto error = errorUtils.createWarning(WARNING_ID);
        //Then
        assertError(null, error, WARNING_ID, ErrorDto.Severity.WARNING);
    }

    @Test
    void createWarningWithAdditionalParameters() {
        //Given
        Map<String, String> additionalParams = getAdditionalProperties();
        //When
        ErrorDto error = errorUtils.createError(WARNING_ID, additionalParams);
        //Then
        assertError(additionalParams, error, WARNING_ID, ErrorDto.Severity.ERROR);
    }

    @Test
    void createGeneralError() {
        //Given
        Map<String, String> additionalParams = getAdditionalProperties();
        //When
        ErrorDto error = errorUtils.createError(Errors.GENERAL_ERROR, additionalParams);
        //Then
        assertError(additionalParams, error, Errors.GENERAL_ERROR.getId(), Errors.GENERAL_ERROR.getSeverity());

    }

    private void assertError(Map<String, String> additionalParams, ErrorDto error, String id, ErrorDto.Severity severity) {
        assertAll(
                () -> assertNotNull(error),
                () -> assertEquals(id, error.getId()),
                () -> assertEquals(severity, error.getSeverity()),
                () -> assertEquals(ResourceBundle.getBundle(ERROR_BUNDLE, Locale.getDefault()).getString(id),
                        error.getMessage()),
                () -> assertEquals(additionalParams, error.getAdditionalProperties())
        );
    }

    private Map<String, String> getAdditionalProperties() {
        Map<String, String> additionalParams = new HashMap<>();
        additionalParams.put(USER_ID, "any user id");
        additionalParams.put(USER_ACTION, "any user action");
        return additionalParams;
    }
}
