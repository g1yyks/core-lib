package ru.grendiser.core.error.dto.error;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import ru.grendiser.core.error.dto.ErrorDto;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ErrorDtoTest {
    
    private static final String ERROR = "{\"id\":\"error code\",\"message\":\"error's message\",\"severity\":\"ERROR\",\"additional properties\":{\"test2\":\"test object 2\",\"test\":\"test object\"}}";
    private static final String ERROR_CODE = "error code";
    private static final String ERROR_MESSAGE = "error's message";
    private static final String FIRST_KEY = "test";
    private static final String SECOND_KEY = "test2";
    private static final String TEST_OBJECT_2 = "test object 2";
    private static final String TEST_OBJECT_1 = "test object";

    @Test
    void serializationToJson() throws JsonProcessingException {
        //Given
        Map<String, String> additionalProperties = getStringObjectMap();
        ErrorDto errorDto = ErrorDto.builder()
                .id(ERROR_CODE)
                .message(ERROR_MESSAGE)
                .severity(ErrorDto.Severity.ERROR)
                .additionalProperties(additionalProperties)
                .build();
        //When
        ObjectMapper mapper = new ObjectMapper();
        //Then
        assertEquals(ERROR, mapper.writeValueAsString(errorDto));
    }


    @Test
    void deserializationToObject() throws JsonProcessingException {
        //Given
        ObjectMapper mapper = new ObjectMapper();
        //When
        ErrorDto error = mapper.readValue(ERROR, ErrorDto.class);
        //Then
        assertAll(
                () -> assertEquals(ERROR_CODE, error.getId()),
                () -> assertEquals(ERROR_MESSAGE, error.getMessage()),
                () -> assertEquals(ErrorDto.Severity.ERROR.getValue(), error.getSeverity().getValue()),
                () -> assertEquals(TEST_OBJECT_1, error.getAdditionalProperties().get(FIRST_KEY)),
                () -> assertEquals(TEST_OBJECT_2, error.getAdditionalProperties().get(SECOND_KEY))
        );
    }

    private Map<String, String> getStringObjectMap() {
        Map<String, String> additionalProperties = new HashMap<>();
        additionalProperties.put(FIRST_KEY, TEST_OBJECT_1);
        additionalProperties.put(SECOND_KEY, TEST_OBJECT_2);
        return additionalProperties;
    }

}