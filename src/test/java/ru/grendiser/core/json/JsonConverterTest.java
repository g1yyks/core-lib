package ru.grendiser.core.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import ru.grendiser.core.error.ErrorUtils;
import ru.grendiser.core.error.Errors;
import ru.grendiser.core.error.dto.ErrorDto;
import ru.grendiser.core.error.impl.ErrorUtilsImpl;
import ru.grendiser.core.json.impl.JsonConverterImpl;

import static org.junit.jupiter.api.Assertions.*;

class JsonConverterTest {

    private final ErrorUtils errorUtils = new ErrorUtilsImpl();
    private final JsonConverter jsonConverter = new JsonConverterImpl();
    private static final String GENERAL_ERROR_JSON = "{\"id\":\"error.general.id\",\"message\":\"Error\",\"severity\":\"ERROR\"}";
    private static final String GENERAL_ERROR_JSON_WITH_ROOT = "{\"test_root_node\":{\"id\":\"error.general.id\",\"message\":\"Error\",\"severity\":\"ERROR\"}}";

    @Test
    void whenErrorDtoIsCorrectFilled_convertToString() {
        //Given
        ErrorDto error = errorUtils.createError(Errors.GENERAL_ERROR, null);
        //When
        String json = jsonConverter.deserialization(error);
        //Then
        assertAll(
                () -> assertNotNull(json),
                () -> assertEquals(GENERAL_ERROR_JSON, json)
        );
    }

    @Test
    void whenErrorDtoIsCorrectFilled_convertToStringWithTestRootName() {
        //Given
        ErrorDto error = errorUtils.createError(Errors.GENERAL_ERROR, null);
        //When
        String json = jsonConverter.deserialization(error, "test_root_node");
        //Then
        assertAll(
                () -> assertNotNull(json),
                () -> assertEquals(GENERAL_ERROR_JSON_WITH_ROOT, json)
        );
    }

    @Test
    void whenJsonStringWithErrorDto_convertToObject() {
        //Given
        //When
        ErrorDto error = jsonConverter.serialization(GENERAL_ERROR_JSON, ErrorDto.class);
        //Then
        assertAll(
                () -> assertNotNull(error),
                () -> assertEquals(Errors.GENERAL_ERROR.getId(), error.getId()),
                () -> assertEquals(Errors.GENERAL_ERROR.getSeverity(), error.getSeverity())
        );
    }

    @Test
    void whenJsonStringWithErrorDtoWithRootNode_convertToObject() {
        //Given
        //When
        TestDtoClass error = jsonConverter.serialization(GENERAL_ERROR_JSON_WITH_ROOT, TestDtoClass.class);
        //Then
        assertAll(
                () -> assertNull(error)
        );
    }

    @AllArgsConstructor
    @Getter @Setter
    class TestDtoClass {
        private String id;
    }

}