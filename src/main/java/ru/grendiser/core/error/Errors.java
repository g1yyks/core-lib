package ru.grendiser.core.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.grendiser.core.error.dto.ErrorDto;

@AllArgsConstructor
@Getter
public enum Errors {

    GENERAL_ERROR("error.general.id", ErrorDto.Severity.ERROR, "{}: general error message - with parameters {}");

    private final String id;
    private final ErrorDto.Severity severity;
    private final String logMessage;
}
