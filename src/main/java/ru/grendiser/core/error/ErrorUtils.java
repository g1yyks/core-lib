package ru.grendiser.core.error;

import ru.grendiser.core.error.dto.ErrorDto;

import java.util.Map;

/**
 * Util class for work with errors
 */
public interface ErrorUtils {

    /**
     * Create error with severity - ERROR and id of error
     * @param errorId id of error
     * @return error object
     */
    ErrorDto createError(String errorId);

    /**
     * Create error with severity - ERROR and id of error and additional params
     * @param errorId id of error
     * @param additionalParams map of additional params
     * @return error object
     */
    ErrorDto createError(String errorId, Map<String, String> additionalParams);

    /**
     * Create error with severity - WARNING and id of error
     * @param errorId id of error
     * @return error object
     */
    ErrorDto createWarning(String errorId);

    /**
     * Create error with severity - WARNING and id of error and additional params
     * @param errorId id of error
     * @param additionalParams map of additional params
     * @return error object
     */
    ErrorDto createWarning(String errorId, Map<String, String> additionalParams);

    /**
     * Create error with severity - WARNING and id of error and additional params
     * @param error Error with id and severity
     * @param additionalParams map of additional params
     * @return error object
     */
    ErrorDto createError(Errors error, Map<String, String> additionalParams);
}
