package ru.grendiser.core.error.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.grendiser.core.error.dto.ErrorDto;
import ru.grendiser.core.error.Errors;
import ru.grendiser.core.error.ErrorUtils;

import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Slf4j
@Service
public class ErrorUtilsImpl implements ErrorUtils {

    private static final String ERROR_BUNDLE = "utils.error";
    private static final String ERROR_LOG_MESSAGE = "{}: Error has occurred with params {}";
    public static final String GENERAL_ERROR_MESSAGE = "Error has occurred";


    @Override
    public ErrorDto createError(String errorId) {
        return createError(errorId, null);
    }

    @Override
    public ErrorDto createError(String errorId, Map<String, String> additionalParams) {
        return getError(errorId, ErrorDto.Severity.ERROR, additionalParams);
    }

    @Override
    public ErrorDto createWarning(String errorId) {
        return createWarning(errorId, null);
    }

    @Override
    public ErrorDto createWarning(String errorId, Map<String, String> additionalParams) {
        return getError(errorId, ErrorDto.Severity.WARNING, additionalParams);
    }

    @Override
    public ErrorDto createError(Errors error, Map<String, String> additionalParams) {
        return getError(error.getId(), error.getSeverity(), additionalParams, error.getLogMessage());
    }

    private ErrorDto getError(String id, ErrorDto.Severity severity, Map<String, String> additionalParams) {
        log.error(ERROR_LOG_MESSAGE, id, additionalParams);
        return ErrorDto.builder()
                .id(id)
                .severity(severity)
                .message(getErrorMessageByCode(id))
                .additionalProperties(additionalParams)
                .build();
    }

    private ErrorDto getError(String id, ErrorDto.Severity severity, Map<String, String> additionalParams, String logMessage) {
        log.error(logMessage, id, additionalParams);
        return ErrorDto.builder()
                .id(id)
                .severity(severity)
                .message(getErrorMessageByCode(id))
                .additionalProperties(additionalParams)
                .build();
    }

    private String getErrorMessageByCode(String errorId) {
        String errorMessage;
        Locale currentLocale = Locale.getDefault();
        try {
            errorMessage = ResourceBundle.getBundle(ERROR_BUNDLE, currentLocale).getString(errorId);
        } catch (MissingResourceException e) {
            errorMessage = GENERAL_ERROR_MESSAGE;
        }

        return errorMessage;
    }
}
