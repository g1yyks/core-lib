
package ru.grendiser.core.error.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "message",
        "severity"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Jacksonized
@Builder @Getter
public class ErrorDto implements Serializable {

    private static final long serialVersionUID = 573345958242940069L;

    /**
     * error's code
     *
     */
    @JsonProperty("id")
    @JsonPropertyDescription("error's code")
    private final String id;

    /**
     * error's description for users
     *
     */
    @JsonProperty("message")
    @JsonPropertyDescription("error's description for users")
    private final String message;

    /**
     * error's type
     *
     */
    @JsonProperty("severity")
    @JsonPropertyDescription("error's type")
    private final Severity severity;

    /**
     * error's type
     *
     */
    @JsonProperty("additional properties")
    @JsonPropertyDescription("additional parameters")
    @JsonSerialize
    private final Map<String, String> additionalProperties;


    /**
     * error's type
     *
     */
    @RequiredArgsConstructor
    @Getter
    public enum Severity {
        ERROR("ERROR"),
        WARNING("WARNING");

        private final String value;

    }

}
