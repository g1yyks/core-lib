package ru.grendiser.core.json.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.grendiser.core.json.JsonConverter;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.UPPER_CAMEL_CASE;

@Slf4j
@Service
public class JsonConverterImpl implements JsonConverter {
    private final ObjectMapper mapper = new ObjectMapper()
            .setPropertyNamingStrategy(UPPER_CAMEL_CASE);

    @Override
    public <T> T serialization(String json, Class<T> tClass) {
        try {
            return mapper.readValue(json, tClass);
        } catch (JsonProcessingException ex) {
            log.error(ex.toString());
        }
        return null;
    }

    @Override
    public <T> String deserialization(T object) {
        return deserialization(object, "");
    }

    @SneakyThrows
    @Override
    public  <T> String deserialization(T object, String rootName) {
        return mapper.writer().withRootName(rootName).writeValueAsString(object);
    }
}
