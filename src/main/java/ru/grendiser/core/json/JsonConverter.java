package ru.grendiser.core.json;

/**
 * Converter for serialization and deserialization jsons
 */
public interface JsonConverter {

    /**
     * Serialization string(json) to object
     * @param json string with json
     * @param <T> type of object
     * @return serialized object
     */
    <T> T serialization(String json, Class<T> tClass);

    /**
     * Deserialization object to string(json)
     * @param object for deserialization
     * @param <T> type of object
     * @return string(json)
     */
    <T> String deserialization(T object);

    /**
     * Deserialization object to string(json)
     * @param object for deserialization
     * @param <T> type of object
     * @param rootName name of root node of json
     * @return string(json)
     */
    <T> String deserialization(T object, String rootName);
}
