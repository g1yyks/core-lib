package ru.grendiser.core.filter.dto;

public enum SortTypeDto {
    DESC, ASC;
}
