package ru.grendiser.core.filter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "field"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Jacksonized
@Getter
@Builder
public class SortDto implements Serializable {
    @JsonProperty("type")
    private final SortTypeDto type;
    @JsonProperty("field")
    private final String field;
}
