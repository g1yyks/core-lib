package ru.grendiser.core.filter.dto;

public enum FilterTypeDto {
    LT,
    LTE,
    GT,
    GTE,
    START_WITH,
    END_WITH,
    EQ,
    CONTAINS,
    IN;
}
