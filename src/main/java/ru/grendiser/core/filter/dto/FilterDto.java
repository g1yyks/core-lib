package ru.grendiser.core.filter.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "field",
        "type",
        "value",
        "values"
})
@Jacksonized
@Builder @Getter
public class FilterDto implements Serializable {
    @JsonProperty("field")
    private final String field;
    @JsonProperty("type")
    private final FilterTypeDto type;
    @JsonProperty("value")
    private final String value;
    @JsonProperty("values")
    private List<String> values;

    public List<String> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return Collections.unmodifiableList(values);
    }
}
