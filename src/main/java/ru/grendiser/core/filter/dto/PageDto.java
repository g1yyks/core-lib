package ru.grendiser.core.filter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sorts",
        "size",
        "number"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Jacksonized
@Getter
@Builder
public class PageDto implements Serializable {
    @JsonProperty("sort")
    private final SortDto sort;
    @JsonProperty("size")
    private final Integer size;
    @JsonProperty("number")
    private final Integer number;
}
