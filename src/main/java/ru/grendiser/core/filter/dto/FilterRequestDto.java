package ru.grendiser.core.filter.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "filters",
        "page"
})
@Jacksonized
@Getter
@Builder
public class FilterRequestDto {
    @JsonProperty("filters")
    private final List<FilterDto> filters;
    @JsonProperty("page")
    private final PageDto page;

    public List<FilterDto> getFilters() {
        return Collections.unmodifiableList(filters);
    }
}
