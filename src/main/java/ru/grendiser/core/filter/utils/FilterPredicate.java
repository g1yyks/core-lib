package ru.grendiser.core.filter.utils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;
import java.util.List;

@SuppressWarnings( "rawtypes")
@FunctionalInterface
public
interface FilterPredicate {

    @SuppressWarnings({"squid:S2326"})
    <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root, String field, List<String> values);
}
