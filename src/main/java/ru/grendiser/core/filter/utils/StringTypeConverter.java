package ru.grendiser.core.filter.utils;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.validator.GenericValidator;

import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static ru.grendiser.core.data.DataPattern.ISO_LOCAL_DATE_PATTERN;
import static ru.grendiser.core.data.DataPattern.ISO_LOCAL_DATE_TIME_PATTERN;


public class StringTypeConverter {

    private StringTypeConverter() {}

    @SuppressWarnings({"unchecked", "rawtype"})
    public static <T extends Comparable> List<T> getValues(Class<?> fieldClazz, List<String> values) {


        if (LocalDate.class.equals(fieldClazz) && values.stream()
                .allMatch(date -> GenericValidator.isDate(date, ISO_LOCAL_DATE_PATTERN, true))) {
            return values.stream().map(date -> (T) LocalDate.parse(date)).collect(Collectors.toList());
        } else

        if (LocalDateTime.class.equals(fieldClazz) && values.stream()
                .allMatch(date -> GenericValidator.isDate(date, ISO_LOCAL_DATE_TIME_PATTERN, false))) {
            return values.stream().map(dateTime -> (T) LocalDateTime.parse(dateTime)).collect(Collectors.toList());
        } else

        if (Long.class.equals(fieldClazz) && values.stream().allMatch(NumberUtils::isDigits)) {
            return values.stream().map(number -> (T) Long.valueOf(number)).collect(Collectors.toList());
        } else

        if (Boolean.class.equals(fieldClazz) && values.stream()
                .allMatch(date -> BooleanUtils.toBooleanObject(date) != null)) {
            return values.stream().map(bool -> (T) Boolean.valueOf(bool)).collect(Collectors.toList());
        }

        return (List<T>) values;

    }

    public static <T> Path<T> getPath(From<?, ?> root, String columnPath) {

        if(!columnPath.contains(".")) {
            return root.get(columnPath);
        }

        String[] levels = columnPath.split("\\.");

        Join<T, Object> join = null;
        Path<T> path = null;
        for(int i = 0; i < levels.length; i++) {
            if (join == null) {
                join = root.join(levels[i]);
            } else if (i == levels.length - 1) {
                path = join.get(levels[i]);
            } else {
                join = join.join(levels[i]);
            }
        }

        return path;
    }
}
