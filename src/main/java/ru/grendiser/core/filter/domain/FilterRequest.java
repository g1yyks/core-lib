package ru.grendiser.core.filter.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class FilterRequest <T extends BaseDomain> implements Specification<T> {

    private List<Filter<T>> filters = new ArrayList<>();
    private PageRequest page;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(filters.stream().map(filter -> filter.toPredicate(root, query, criteriaBuilder)).toArray(Predicate[]::new));
    }
}
