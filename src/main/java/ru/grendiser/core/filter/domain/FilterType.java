package ru.grendiser.core.filter.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.grendiser.core.filter.utils.FilterPredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static ru.grendiser.core.filter.utils.StringTypeConverter.getPath;
import static ru.grendiser.core.filter.utils.StringTypeConverter.getValues;

@SuppressWarnings({"unchecked", "rawtypes"})
@Slf4j
@Getter
@AllArgsConstructor
public enum FilterType implements FilterPredicate {
    LT {
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);
            return cb.lessThan(pathAndValue.getKey(), pathAndValue.getValue().get(0));
        }
    },
    LTE{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);

            return cb.lessThanOrEqualTo(pathAndValue.getKey(), pathAndValue.getValue().get(0));
        }
    },
    GT{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);

            return cb.greaterThan(pathAndValue.getKey(), pathAndValue.getValue().get(0));
        }
    },
    GTE{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);

            return cb.greaterThanOrEqualTo(pathAndValue.getKey(), pathAndValue.getValue().get(0));
        }
    },
    START_WITH{
        @SuppressWarnings({"squid:S2326"})
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<String>, List<String>> pathAndValue = getPathAndValue(field, root, values);
            return cb.like(pathAndValue.getKey(), pathAndValue.getValue().get(0) + "%");
        }
    },
    END_WITH{
        @SuppressWarnings({"squid:S2326"})
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<String>, List<String>> pathAndValue = getPathAndValue(field, root, values);
            return cb.like(pathAndValue.getKey(), "%" +  pathAndValue.getValue().get(0));
        }
    },
    EQ{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);

            return cb.equal(pathAndValue.getKey(), pathAndValue.getValue());
        }
    },
    CONTAINS{
        @SuppressWarnings({"squid:S2326"})
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<String>, List<String>> pathAndValue = getPathAndValue(field, root, values);
            return cb.like(pathAndValue.getKey(), "%" +  pathAndValue.getValue().get(0) + "%");
        }
    },
    IN{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root,
                                                            String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);
            return pathAndValue.getKey().in(pathAndValue.getValue());
        }
    },
    BETWEEN{
        @Override
        public <R extends Comparable> Predicate toPredicate(CriteriaBuilder cb, From<?, ?> root, String field, List<String> values) {
            Map.Entry<Path<R>, List<R>> pathAndValue = getPathAndValue(field, root, values);
            if (pathAndValue.getValue() == null || pathAndValue.getValue().size() < 2) {
                throw new NoSuchElementException();
            }
            R startValue = (R) Collections.min(pathAndValue.getValue());
            R finishValue = (R) Collections.max(pathAndValue.getValue());
            return cb.between(pathAndValue.getKey(), startValue, finishValue);
        }
    }
    ;

    private static <R extends Comparable> Map.Entry<Path<R>, List<R>> getPathAndValue(String fieldPath, From<?, ?> root, List<String> values) {
        Path<R> rootPath = getPath(root, fieldPath);
        Class<?> fieldClass = rootPath.getModel().getBindableJavaType();

        List<R> convertedValue = getValues(fieldClass, values);

        return Collections.singletonMap(rootPath, convertedValue).entrySet().stream().findFirst().orElseThrow();
    }
}
