package ru.grendiser.core.filter.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter @Setter
@MappedSuperclass
public class BaseDomain implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
}
