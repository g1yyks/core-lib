package ru.grendiser.core.filter.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class Filter <T extends BaseDomain> implements  Specification<T> {
    private String field;
    private FilterType type;
    private String value;
    private List<String> values = new ArrayList<>();

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return type.toPredicate(criteriaBuilder, root, field, value != null ? List.of(value): values);
    }
}
