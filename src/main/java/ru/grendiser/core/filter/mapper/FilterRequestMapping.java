package ru.grendiser.core.filter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import ru.grendiser.core.filter.domain.FilterRequest;
import ru.grendiser.core.filter.dto.FilterRequestDto;

@SuppressWarnings({"squid:S1214", "squid:S3740"})
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = PageMapper.class)
public interface FilterRequestMapping {

    FilterRequestMapping MAPPER = Mappers.getMapper(FilterRequestMapping.class);

    @Mapping(target = "filters", source = "filters")
    @Mapping(target = "page", source = "page")
    FilterRequest dtoToDomain(FilterRequestDto filterRequest);
}
