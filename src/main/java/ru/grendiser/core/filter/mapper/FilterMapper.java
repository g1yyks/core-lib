package ru.grendiser.core.filter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import ru.grendiser.core.filter.domain.Filter;
import ru.grendiser.core.filter.dto.FilterDto;

@SuppressWarnings({"squid:S1214", "squid:S3740"})
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FilterMapper {

    FilterMapper MAPPER = Mappers.getMapper(FilterMapper.class);

    @Mapping(target = "field", source = "field")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "value", source = "value")
    @Mapping(target = "values", source = "values")
    Filter dtoToDomain(FilterDto filter);
}
