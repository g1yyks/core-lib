package ru.grendiser.core.filter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Sort;
import ru.grendiser.core.filter.dto.SortDto;

@SuppressWarnings("squid:S1214")
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class SortMapper {

    public static final SortMapper MAPPER = Mappers.getMapper(SortMapper.class);

    Sort dtoToDomain(SortDto sort) {
        return Sort.by(Sort.Direction.fromString(sort.getType().name()), sort.getField());
    }
}
