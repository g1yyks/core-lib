package ru.grendiser.core.filter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ru.grendiser.core.filter.dto.PageDto;

@SuppressWarnings("squid:S1214")
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = SortMapper.class)
public abstract class PageMapper {

    public static final PageMapper MAPPER = Mappers.getMapper(PageMapper.class);

    PageRequest dtoToDomain(PageDto pageDto) {
        Sort sort = SortMapper.MAPPER.dtoToDomain(pageDto.getSort());
        return PageRequest.of(pageDto.getNumber(), pageDto.getSize(), sort);
    }
}
