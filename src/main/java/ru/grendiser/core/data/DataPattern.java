package ru.grendiser.core.data;

/**
 * Contains date and date time patterns
 */
public final class DataPattern {

    private DataPattern() {}

    public static final String ISO_LOCAL_DATE_PATTERN = "yyyy-MM-dd";
    public static final String ISO_LOCAL_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
}
