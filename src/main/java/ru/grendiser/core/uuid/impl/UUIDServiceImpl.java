package ru.grendiser.core.uuid.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.grendiser.core.uuid.UUIDService;

import java.util.UUID;

@Slf4j
@Service
public class UUIDServiceImpl implements UUIDService {

    @Override
    public UUID generateUUID(String uuid) {
        if (!StringUtils.hasLength(uuid)) {
            return UUID.randomUUID();
        }

        try {
            return UUID.fromString(uuid);
        } catch (IllegalArgumentException ex) {
            log.warn(ex.toString());
            return UUID.randomUUID();
        }
    }
}
