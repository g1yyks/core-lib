package ru.grendiser.core.uuid;

import java.util.UUID;

public interface UUIDService {
    /**
     * Parse string uuid or generate new
     * @param uuid as string
     * @return parsed or generated random uuid
     */
    UUID generateUUID(String uuid);
}
